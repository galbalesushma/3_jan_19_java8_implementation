package com.agile.operations;

import java.util.List;

public class Functional_Programming {

	
	 public static String show(String message){  
	        return "Hello "+message;  
	    }
	 
		public static Integer add(List<Integer> list)
		{
			   return list.stream()  
	                   .mapToInt(Integer::intValue).sum();  
			
		}
}
