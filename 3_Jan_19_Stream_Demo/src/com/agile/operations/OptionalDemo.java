package com.agile.operations;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

public class OptionalDemo {

	// want to filter the list of strings and get a value whose length is greater
	// than a threshold value.

	public void filterList() {
		List<String> list = Arrays.asList("Rekha", "Jaya", "Sushama", "Gosling");
		Optional<String> largeString = list.stream().filter(str -> str.length() > 10).findAny();
		largeString.ifPresent(System.out::println);

		Optional<String> veryLargeString = list.stream().filter(str -> str.length() > 20).findAny();
		veryLargeString.ifPresent(System.out::println);

		System.out.println(largeString.orElse("No string > than 10") + veryLargeString.orElse("\tno string > 20"));

		System.out.println(largeString.orElseGet(() -> "Default Value"));

	}

}
