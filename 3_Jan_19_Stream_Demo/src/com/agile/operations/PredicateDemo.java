package com.agile.operations;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;



public class PredicateDemo {
public static Boolean checkAge(int age)
{
	if(age>18)
	return true;
	else return false;
}


public static void startWith_G_Strings()
{
	 List<String> names = 
	            Arrays.asList("Gk","GKQuiz","g1","QA","Gk2"); 
	  
	       
	        Predicate<String> p = (s)->s.startsWith("G"); 
	  
	        for (String st:names) 
	        { 
	            if (p.test(st)) 
	                System.out.println(st); 
	        } 
	    } 

}
