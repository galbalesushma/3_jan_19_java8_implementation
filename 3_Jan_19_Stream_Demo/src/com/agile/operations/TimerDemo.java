package com.agile.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class TimerDemo {

	private void processData(List<Integer> i) {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	

	public void timeToIterate_forEach() {

		List<Integer> list = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			int n = 1 + (int) (Math.random() * 100);
			list.add(n);
		}
		System.out.println("------------------------------------------------------------------------------");
		long start1 = System.currentTimeMillis();
		System.out.println(start1 + " ms");
		for (Integer i : list) {

		}
		long end1 = System.currentTimeMillis();
		System.out.println(end1 + " ms");
		System.out.println("Iterating 10E6 elements using for loop takes " + (end1 - start1) + "ms");
		System.out.println("------------------------------------------------------------------------------");

		long start2 = System.currentTimeMillis();
		list.forEach(value -> {
		});
		long end2 = System.currentTimeMillis();
		System.out.println("Iterating 10E6 elements using forEach takes " + (end2 - start2) + "ms");
		System.out.println("------------------------------------------------------------------------------");

		long start3 = System.currentTimeMillis();
		Stream.of(list).forEach(i -> processData(i));
		long end3 = System.currentTimeMillis();
		System.out.println("Iterating 10E6 elements using Stream forEach takes " + (end3 - start3) + "ms");
		System.out.println("------------------------------------------------------------------------------");

		Stream parallelStream = Arrays.asList(list).parallelStream();
		long start4 = System.currentTimeMillis();
		Stream.of(list).parallel().forEach(i -> processData(i));
		long end4 = System.currentTimeMillis();

		System.out.println("Iterating 10E6 elements using parallelStream forEach takes " + (end4 - start4) + "ms");
		System.out.println("------------------------------------------------------------------------------");

		

		long start5 = System.currentTimeMillis();
		list.stream().flatMap(val -> Stream.of(val)).forEach(value->{});
		long end5 = System.currentTimeMillis();

		System.out.println("Iterating 10E6 elements using flatMap forEach takes " + (end5 - start5) + "ms");
		System.out.println("------------------------------------------------------------------------------");
	}

}
